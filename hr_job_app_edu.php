<?php
//session_start();
//connect with database
include "../db/connection.php";

?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Form start  -->




 <div class="col-md-12"><!--Academic Qualification Start hare-->
	
<h4 style="background-color: #11638B; color: white; padding: 5px 5px; ">Academic Qualification</h4>
	
</div>




<form action="hr_job_app_edu_ins.php" method="post" enctype="multipart/form-data">

 <div class="col-md-12" align="center">  
<table class="table table-bordered">

    <thead style="text-align: center;">
      <tr>
        <th>Level</th>
        <th>Name Of Examination</th>
        <th>Passing Year</th>
        <th>Subject/Group</th>
        <th colspan="2">Result</th>
        <th>School/College/Institute</th>
        <th>Board/University</th>
       
      </tr>
    </thead>
    <tbody>

      <tr>
      <td>SSC/Equivalent</td>
       <td><select name="ssc_equivalent" id="ssc_equivalent" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="SSC">SSC</option>
        	<option value="O Level">O Level</option>
        	<option value="Dakhil">Dakhil</option>
        </select></td>

        <td><select name="ssc_passing_year" id="ssc_passing_year" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="2010">2010</option>
        	<option value="2011">2011</option>
        	<option value="2012">2012</option>
        	<option value="2013">2013</option>
        	<option value="2014">2014</option>
        	<option value="2015">2015</option>
        	<option value="2016">2016</option>
        	<option value="2017">2017</option>
        </select></td> 
        <td><select name="ssc_subject_group" id="ssc_subject_group" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="Science">Science</option>
        	<option value="Arts">Arts</option>
        	<option value="Commerce">Commerce</option>
        </select></td> 
        
        
        <td><select name="ssc_result_cgpa_gpa" id="ssc_result_cgpa_gpa" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="Division">Division</option>
        	<option value="Grade">Grade</option>
        </select></td> 
        
        <td><select name="ssc_result_cgpa_gpa1" id="ssc_result_cgpa_gpa1" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="First">First</option>
        	<option value="Second">Second</option>
        	<option value="Third">Third</option>

        </select></td>  
        <td><input type="text" name="ssc_school_college_institute" id="ssc_school_college_institute"></td> 
        <td><input type="text" name="ssc_board" id="ssc_board"></td> 
      </tr>
     
      <!--HSC from Bellow--> 
      
      <tr>
        <td>HSC/Equivalent</td>
        <td><select name="hsc_equivalent" id="hsc_equivalent" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="HSC">HSC</option>
        	<option value="A Level">A Level</option>
        	<option value="Alim">Alim</option>
        	<option value="Diploma">Diploma</option>
        </select></td>

        <td><select name="hsc_passing_year" id="hsc_passing_year" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="2010">2010</option>
        	<option value="2011">2011</option>
        	<option value="2012">2012</option>
        	<option value="2013">2013</option>
        	<option value="2014">2014</option>
        	<option value="2015">2015</option>
        	<option value="2016">2016</option>
        	<option value="2017">2017</option>
        </select></td> 
        <td><select name="hsc_subject_group" id="hsc_subject_group" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="Science">Science</option>
        	<option value="Arts">Arts</option>
        	<option value="Commerce">Commerce</option>
        </select></td> 
        
        
        <td><select name="hsc_result_cgpa_gpa" id="hsc_result_cgpa_gpa" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="">Division</option>
        	<option value="">Grade</option>
        </select></td> 
		
        <!--hsc_result_cgpa_gpa_point to be added a new column in database -->
		
        <td><select name="hsc_result_cgpa_gpa1" id="hsc_result_cgpa_gpa1" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="First">First</option>
        	<option value="Second">Second</option>
        	<option value="Third">Third</option>
        	
        </select></td>  
        
        <td><input type="text" name="hsc_school_college_institute" id="hsc_school_college_institute"></td> 
        <td><input type="text" name="hsc_board" id="hsc_board"></td> 
      </tr>
      
      
   <!--Graduation from Bellow-->   
    <tr>
        <td>Graduate</td>
        <td><select name="graduate" id="graduate" class="form-control form-control-sm" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="B.Arch">B.Arch</option>
        	<option value="Bachelor(Pass)">Bachelor(Pass)</option>
        	<option value="Bachelor(Hons)">Bachelor(Hons)</option>
        	<option value="BBA">BBA</option>
        	<option value="Bsc. Engineering">Bsc. Engineering</option>
        	<option value="Fazil">Fazil</option>
        	<option value="Others">Others</option>
        </select></td>

        <td><select name="graduate_passing_year" id="graduate_passing_year" class="form-control form-control-sm" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="2010">2010</option>
        	<option value="2011">2011</option>
        	<option value="2012">2012</option>
        	<option value="2013">2013</option>
        	<option value="2014">2014</option>
        	<option value="2015">2015</option>
        	<option value="2016">2016</option>
        	<option value="2017">2017</option>
        </select></td> 
        <td><select name="graduate_subject_group" id="graduate_subject_group" class="form-control form-control-sm" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="Accounting">Accounting</option>
        	<option value="Bangla">Bangla</option>
        	<option value="Accounting and Finance">Accounting and Finance</option>
        	<option value="Marketing">Marketing</option>
        	<option value="English">English</option>
        	<option value="Economics">Economics</option>
        	<option value="Fine Arts">Fine Arts</option>
        	<option value="Fisharies">Fisharies</option>
        	<option value="Physics">Physics</option>
        	<option value="Statistics">Statistics</option>
        	<option value="Arabic">Arabic</option>
        	<option value="Mathmatics">Mathmatics</option>
        </select></td> 
        
        
        <td><select name="graduate_result_cgpa_gpa" id="graduate_result_cgpa_gpa" class="form-control form-control-sm" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="Division">Division</option>
        	<option value="Grade">Grade</option>
        </select></td> 
		
        <!--graduate_result_cgpa_gpa1 to be added in the database -->
		
        <td><select name="graduate_result_cgpa_gpa1" id="graduate_result_cgpa_gpa1" class="form-control form-control-sm" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="First">First</option>
        	<option value="Second">Second</option>
        	<option value="Third">Third</option>
        	<option value="Point">Point</option>
        </select></td>  
        <td><input type="text" name="graduate_school_college_institute" id="graduate_school_college_institute"></td> 
        <td><input type="text" name="graduate_board_university" id="graduate_board_university"></td> 
		<!--University list dropdown  to be added here-->
      </tr> 

	  
    <!--Post Graduation From Bellow-->  
     <tr>
        <td>Post Graduation</td>
        <td><select name="post_graduate" id="post_graduate" class="form-control form-control-sm" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="B.Arch">M.Arch</option>
        	<option value="Master(Pass">Master(Pass)</option>
        	<option value="Master">Master</option>
        	<option value="MBA">BBA</option>
        	<option value="Msc. in Engineering">Msc. in Engineering</option>
        	<option value="">Fazil</option>
        	<option value="">Others</option>
        </select></td>

        <td><select name="post_graduate_passing_year" id="post_graduate_passing_year" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="2010">2010</option>
        	<option value="2011">2011</option>
        	<option value="2012">2012</option>
        	<option value="2013">2013</option>
        	<option value="2014">2014</option>
        	<option value="2015">2015</option>
        	<option value="2016">2016</option>
        	<option value="2017">2017</option>
        </select></td> 
       <td><select name="post_graduate_subject_group" id="post_graduate_subject_group" class="form-control form-control-sm" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="Accounting">Accounting</option>
        	<option value="Bangla">Bangla</option>
        	<option value="Accounting and Finance">Accounting and Finance</option>
        	<option value="Marketing">Marketing</option>
        	<option value="English">English</option>
        	<option value="Economics">Economics</option>
        	<option value="Fine Arts">Fine Arts</option>
        	<option value="Fisharies">Fisharies</option>
        	<option value="Physics">Physics</option>
        	<option value="Statistics">Statistics</option>
        	<option value="Arabic">Arabic</option>
        	<option value="Mathmatics">Mathmatics</option>
        	<option value="MBA">MBA</option>
        </select></td> 
        
        
        <td><select name="post_graduate_result_cgpa_gpa" id="post_graduate_result_cgpa_gpa" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="">Division</option>
        	<option value="">Grade</option>
        </select></td> 
        
        <td><select name="post_graduate_result_cgpa_gpa1" id="post_graduate_result_cgpa_gpa1" class="form-control form-control-sm">
        <option value="">--Select--</option>
        	<option value="First">First</option>
        	<option value="Second">Second</option>
        	<option value="Third">Third</option>
        	<option value="Point">Point</option>
        </select></td>  
        <td><input type="text" name="post_graduate_school_college_institute" id="post_graduate_school_college_institute" class="form-control"></td> 
        <td><input type="text" name="post_graduate_board_university" id="post_graduate_board_university" class="form-control" ></td> 
      </tr> 
    </tbody>
  </table>   

<input type="submit" name="submit" value="Next"  />

</form>










<!--PHP code begining here -->

<?php

//after id add new coloum job_app_id column 
//print $_GET['job_app_id'];

/*
id
job_app_id
exam_type
exam_result
exam_year
exam_ins

1	10	ssc	1st	2000	rajshahi board
2	10	hsc
*/
?>



<!-- PHP code from here -->


</div>



