<?php
//session_start();
//connect with database
include "../db/connection.php";
include "css/cssheader.php";
?>

            <?php
//query to retrieve data 
            $result = mysqli_query($con, "select * from school_job_app_edu where id=1);");
//fetch data from result set
			$row = mysqli_fetch_array($result);
            
                ?> 



 <div class="col-md-12"><!--Academic Qualification Start hare-->
	
<h4 style="background-color: #11638B; color: white; padding: 5px 5px; ">Academic Qualification</h4>
	
</div>
 <div class="col-md-12" align="center">
     
<form action="" method="post" enctype="multipart/form-data">
      
<table class="table table-bordered">
    <thead style="text-align: center;">
      <tr>
        <th>Level</th>
        <th>Name Of Examination</th>
        <th>Passing Year</th>
        <th>Subject/Group</th>
        <th colspan="2">Result</th>
        <th>School/College/Institute</th>
        <th>Board/University</th>
       
      </tr>
    </thead>
    <tbody>
      <tr>
      <td>SSC/Equivalent</td>
       <td><select name="ssc_equivalent" id="ssc_equivalent" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['ssc_equivalent']; ?></option>
        	<option value="SSC">SSC</option>
        	<option value="O Level">O Level</option>
        	<option value="Dakhil">Dakhil</option>
        </select></td>

        <td><select name="ssc_passing_year" id="ssc_passing_year" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['ssc_passing_year']; ?></option>
        	<option value="2010">2007</option>
        	<option value="2011">2008</option>
        	<option value="2012">2009</option>
        	<option value="2010">2010</option>
        	<option value="2011">2011</option>
        	<option value="2012">2012</option>
        	<option value="2013">2013</option>
        	<option value="2014">2014</option>
        	<option value="2015">2015</option>
        	<option value="2016">2016</option>
        	<option value="2017">2017</option>
        </select></td> 
        <td><select name="ssc_subject_group" id="ssc_subject_group" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['ssc_subject_group']; ?></option>
        	<option value="Science">Science</option>
        	<option value="Arts">Arts</option>
        	<option value="Commerce">Commerce</option>
        </select></td> 
        
        
        <td><select name="ssc_result_cgpa_gpa" id="ssc_result_cgpa_gpa" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['ssc_result_cgpa_gpa']; ?></option>
        	<option value="Division">Division</option>
        	<option value="Grade">Grade</option>
        </select></td> 
        
        <td><select name="ssc_result_cgpa_gpa1" id="ssc_result_cgpa_gpa1" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['ssc_result_cgpa_gpa1']; ?></option>
        	<option value="First">First</option>
        	<option value="Second">Second</option>
        	<option value="Third">Third</option>
        </select></td>  
        <td><input type="text" name="ssc_school_college_institute" id="ssc_school_college_institute" value="<?php print $row['ssc_school_college_institute']; ?>"></td> 
        <td><input type="text" name="ssc_board" id="ssc_board" value="<?php print $row['ssc_board']; ?>"></td> 
      </tr>
     
      <!--HSC from Bellow--> 
      
      <tr>
        <td>HSC/Equivalent</td>
        <td><select name="hsc_equivalent" id="hsc_equivalent" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['hsc_equivalent']; ?></option>
        	<option value="HSC">HSC</option>
        	<option value="A Level">A Level</option>
        	<option value="Alim">Alim</option>
        	<option value="Diploma">Diploma</option>
        </select></td>

        <td><select name="hsc_passing_year" id="hsc_passing_year" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['hsc_passing_year'];?></option>
        	<option value="2010">2010</option>
        	<option value="2011">2011</option>
        	<option value="2012">2012</option>
        	<option value="2013">2013</option>
        	<option value="2014">2014</option>
        	<option value="2015">2015</option>
        	<option value="2016">2016</option>
        	<option value="2017">2017</option>
        </select></td> 
        <td><select name="hsc_subject_group" id="hsc_subject_group" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['hsc_subject_group'];?></option>
        	<option value="Science">Science</option>
        	<option value="Arts">Arts</option>
        	<option value="Commerce">Commerce</option>
        </select></td> 
        
        
        <td><select name="hsc_result_cgpa_gpa" id="hsc_result_cgpa_gpa" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['hsc_result_cgpa_gpa']; ?></option>
        	<option value="">Division</option>
        	<option value="">Grade</option>
        </select></td> 
		
        <!--hsc_result_cgpa_gpa_point to be added a new column in database -->
		
        <td><select name="hsc_result_cgpa_gpa1" id="hsc_result_cgpa_gpa1" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['hsc_result_cgpa_gpa1']; ?></option>
        	<option value="First">First</option>
        	<option value="Second">Second</option>
        	<option value="Third">Third</option>
        	
        </select></td>  
        
        <td><input type="text" name="hsc_school_college_institute" id="hsc_school_college_institute"value="<?php print $row['hsc_school_college_institute']; ?>"></td> 
        <td><input type="text" name="hsc_board" id="hsc_board" value="<?php print $row['hsc_board']; ?>"></td> 
      </tr>
      
      
   <!--Graduation from Bellow-->   
    <tr>
        <td>Graduate</td>
        <td><select name="graduate" id="graduate" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['graduate']; ?></option>
        	<option value="B.Arch">B.Arch</option>
        	<option value="Bachelor(Pass)">Bachelor(Pass)</option>
        	<option value="Bachelor(Hons)">Bachelor(Hons)</option>
        	<option value="BBA">BBA</option>
        	<option value="Bsc. Engineering">Bsc. Engineering</option>
        	<option value="Fazil">Fazil</option>
        	<option value="Others">Others</option>
        </select></td>

        <td><select name="graduate_passing_year" id="graduate_passing_year" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['graduate_passing_year']; ?></option>
        	<option value="2010">2010</option>
        	<option value="2011">2011</option>
        	<option value="2012">2012</option>
        	<option value="2013">2013</option>
        	<option value="2014">2014</option>
        	<option value="2015">2015</option>
        	<option value="2016">2016</option>
        	<option value="2017">2017</option>
        </select></td> 
        <td><select name="graduate_subject_group" id="graduate_subject_group"  class="form-control form-control-sm" value="">
        <option value=""><?php print $row['graduate_subject_group']; ?></option>
        	<option value="Accounting">Accounting</option>
        	<option value="Bangla">Bangla</option>
        	<option value="Accounting and Finance">Accounting and Finance</option>
        	<option value="Marketing">Marketing</option>
        	<option value="English">English</option>
        	<option value="Economics">Economics</option>
        	<option value="Fine Arts">Fine Arts</option>
        	<option value="Fisharies">Fisharies</option>
        	<option value="Physics">Physics</option>
        	<option value="Statistics">Statistics</option>
        	<option value="Arabic">Arabic</option>
        	<option value="Mathmatics">Mathmatics</option>
        </select></td> 
        
        
        <td><select name="graduate_result_cgpa_gpa" id="graduate_result_cgpa_gpa"  class="form-control form-control-sm" value="">
        <option value=""><?php print $row['graduate_result_cgpa_gpa']; ?></option>
        	<option value="Division">Division</option>
        	<option value="Grade">Grade</option>
        </select></td> 
		
        <!--graduate_result_cgpa_gpa_point to be added in the database -->
		
        <td><select name="graduate_result_cgpa_gpa1" id="graduate_result_cgpa_gpa1" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['graduate_result_cgpa_gpa1']; ?></option>
        	<option value="First">First</option>
        	<option value="Second">Second</option>
        	<option value="Third">Third</option>
        	<option value="Point">Point</option>
        </select></td> 
         
        <td><input type="text" name="graduate_school_college_institute" id="graduate_school_college_institute" value="<?php print $row['graduate_school_college_institute']; ?>"></td> 
        
        <td><input type="text" name="graduate_board_university" id="graduate_board_university" value="<?php print $row['graduate_board_university']; ?>"></td> 
		<!--University list dropdown  to be added here-->
      </tr> 

	  
    <!--Post Graduation From Bellow-->  
     <tr>
        <td>Post Graduation</td>
        <td><select name="post_graduate" id="post_graduate" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['post_graduate']; ?></option>
        	<option value="B.Arch">M.Arch</option>
        	<option value="Master(Pass">Master(Pass)</option>
        	<option value="Master">Master</option>
        	<option value="MBA">BBA</option>
        	<option value="Msc. in Engineering">Msc. in Engineering</option>
        	<option value="">Fazil</option>
        	<option value="">Others</option>
        </select></td>

        <td><select name="post_graduate_passing_year" id="post_graduate_passing_year" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['post_graduate_passing_year']; ?></option>
        	<option value="2010">2010</option>
        	<option value="2011">2011</option>
        	<option value="2012">2012</option>
        	<option value="2013">2013</option>
        	<option value="2014">2014</option>
        	<option value="2015">2015</option>
        	<option value="2016">2016</option>
        	<option value="2017">2017</option>
        </select></td> 
       <td><select name="post_graduate_subject_group" id="post_graduate_subject_group" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['post_graduate_subject_group']; ?></option>
        	<option value="Accounting">Accounting</option>
        	<option value="Bangla">Bangla</option>
        	<option value="Accounting and Finance">Accounting and Finance</option>
        	<option value="Marketing">Marketing</option>
        	<option value="English">English</option>
        	<option value="Economics">Economics</option>
        	<option value="Fine Arts">Fine Arts</option>
        	<option value="Fisharies">Fisharies</option>
        	<option value="Physics">Physics</option>
        	<option value="Statistics">Statistics</option>
        	<option value="Arabic">Arabic</option>
        	<option value="Mathmatics">Mathmatics</option>
        	<option value="MBA">MBA</option>
        </select></td> 
        
        
        <td><select name="post_graduate_result_cgpa_gpa" id="post_graduate_result_cgpa_gpa" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['post_graduate_result_cgpa_gpa']; ?></option>
        	<option value="">Division</option>
        	<option value="">Grade</option>
        </select></td> 
        
        <td><select name="post_graduate_result_cgpa_gpa1" id="post_graduate_result_cgpa_gpa1" class="form-control form-control-sm" value="">
        <option value=""><?php print $row['post_graduate_result_cgpa_gpa1']; ?></option>
        	<option value="First">First</option>
        	<option value="Second">Second</option>
        	<option value="Third">Third</option>
        	<option value="Point">Point</option>
        </select></td>  
        <td><input type="text" name="post_graduate_school_college_institute" id="post_graduate_school_college_institute" class="form-control" value="<?php print $row['post_graduate_school_college_institute']; ?>"></td>
         
    <td><input type="text" name="post_graduate_board_university" id="post_graduate_board_university" class="form-control" value="<?php print $row['post_graduate_board_university']; ?>"></td> 
      </tr> 
    </tbody>
  </table>
    <div>
    <input type="submit" name="update" id="update" value="Update"
    </div>
</form>
  </div> <!--Education Table Closing Here--> 





<?php
//insertion



if(isset($_POST['ssc_equivalent'])){

	// $job_app_id = $_POST['job_app_id'];
	
	
	 $ssc_equivalent = $_POST['ssc_equivalent'];
	 $ssc_passing_year = $_POST['ssc_passing_year'];
	 $ssc_subject_group = $_POST['ssc_subject_group'];
	 $ssc_result_cgpa_gpa = $_POST['ssc_result_cgpa_gpa'];
	 $ssc_result_cgpa_gpa1 = $_POST['ssc_result_cgpa_gpa1'];
	 $ssc_school_college_institute = $_POST['ssc_school_college_institute'];
	 $ssc_board = $_POST['ssc_board'];
	/*HSC code Start from here*/
	 $hsc_equivalent = $_POST['hsc_equivalent'];
	 $hsc_passing_year = $_POST['hsc_passing_year'];
	 $hsc_subject_group = $_POST['hsc_subject_group'];
	 $hsc_result_cgpa_gpa = $_POST['hsc_result_cgpa_gpa'];
	 $hsc_result_cgpa_gpa1 = $_POST['hsc_result_cgpa_gpa1'];
	 $hsc_school_college_institute = $_POST['hsc_school_college_institute'];
	 $hsc_board = $_POST['hsc_board'];
	
	/*Graduation code Start from here*/
	 $graduate = $_POST['graduate'];
	 $graduate_passing_year = $_POST['graduate_passing_year'];
	 $graduate_subject_group = $_POST['graduate_subject_group'];
	 $graduate_result_cgpa_gpa = $_POST['graduate_result_cgpa_gpa'];
	 $graduate_result_cgpa_gpa1 = $_POST['graduate_result_cgpa_gpa1'];
	 $graduate_school_college_institute =  $_POST['graduate_school_college_institute'];
	 $graduate_board_university = $_POST['graduate_board_university'];
	
	/*Post Graduation code Start from here*/
	 $post_graduate = $_POST['post_graduate'];
	 $post_graduate_passing_year = $_POST['post_graduate_passing_year'];
	 $post_graduate_subject_group = $_POST['post_graduate_subject_group'];
	 $post_graduate_result_cgpa_gpa = $_POST['post_graduate_result_cgpa_gpa'];
	 $post_graduate_result_cgpa_gpa1 = $_POST['post_graduate_result_cgpa_gpa1'];
	 $post_graduate_school_college_institute = $_POST['post_graduate_school_college_institute'];
	 $post_graduate_board_university = $_POST['post_graduate_board_university'];

$sql_ins="update school_job_app_edu set 
ssc_equivalent='$ssc_equivalent',
ssc_passing_year='$ssc_passing_year',
ssc_subject_group='$ssc_subject_group',
ssc_result_cgpa_gpa='$ssc_result_cgpa_gpa',
ssc_result_cgpa_gpa1='$ssc_result_cgpa_gpa1',
ssc_school_college_institute='$ssc_school_college_institute',
ssc_board='$ssc_board',

hsc_equivalent='$hsc_equivalent',
hsc_passing_year='$hsc_passing_year',
hsc_subject_group='$hsc_subject_group',
hsc_result_cgpa_gpa='$hsc_result_cgpa_gpa',
hsc_result_cgpa_gpa1='$hsc_result_cgpa_gpa1',
hsc_school_college_institute='$hsc_school_college_institute',
hsc_board='$hsc_board',

graduate='$graduate',
graduate_passing_year='$graduate_passing_year',
graduate_subject_group='$graduate_subject_group',
graduate_result_cgpa_gpa='$graduate_result_cgpa_gpa',
graduate_result_cgpa_gpa1='$graduate_result_cgpa_gpa1',
graduate_school_college_institute='$graduate_school_college_institute',
graduate_board_university='$graduate_board_university',

post_graduate='$post_graduate',
post_graduate_passing_year='$post_graduate_passing_year',
post_graduate_subject_group='$post_graduate_subject_group',
post_graduate_result_cgpa_gpa='$post_graduate_result_cgpa_gpa',
post_graduate_result_cgpa_gpa1='$post_graduate_result_cgpa_gpa1',
post_graduate_school_college_institute='$post_graduate_school_college_institute',
post_graduate_board_university='$post_graduate_board_university',

where id= 1);";

	
mysqli_query($con,$sql_ins);	

//query execution
/*$result_ins=mysqli_query($con,$sql_ins);
$q = "select max(id) from school_job_app_edu";
$q1 = mysqli_query($con,$q);
$q2 = mysqli_fetch_row($q1);*/
// $id= $q2[0];
	// $job_app_id= $q2[1];
?> 
<script>
window.location ="hr_job_app_exp.php?job_app_id="+
<?php // $id; ?>;
<?php // $job_app_id; ?>;



</script>
<?php
}
?>